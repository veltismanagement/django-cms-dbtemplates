#!/usr/bin/env python
import os
from setuptools import setup, find_packages

README_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                           'README.rst')

dependencies = [
    'django-cms==2.4.3',
    'djangotoolbox',
    'django-dbtemplates==1.4.2pbs.2',
    'python-graph-core',
    'django-admin-extend',
]


dependency_links = [
    'http://github.com/pbs/django-dbtemplates/tarball/master#egg=django-dbtemplates-1.4.2pbs.2',
    # 'http://github.com/pbs/django-cms/tarball/support/2.3.x#egg=django-cms-2.3.5pbs',
    'http://github.com/pbs/django-admin-extend/tarball/master#egg=django-admin-extend-0.1',
]


setup(
    name='django-cms-dbtemplates',
    version='0.14tmr.0.3',
    description='Integrate django-cms and django-dbtemplates',
    long_description=open(README_PATH, 'r').read(),
    author='Sever Banesiu',
    author_email='banesiu.sever@gmail.com',
    url='https://github.com/pbs/django-cms-dbtemplates',
    packages=find_packages(),
    include_package_data=True,
    install_requires=dependencies,
    dependency_links=dependency_links,
    setup_requires=[
        's3sourceuploader',
    ],
    tests_require=[
        'parse',
        'django-nose',
        'mock==1.0.1',
    ],
    test_suite='runtests.runtests',
)
